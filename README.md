# caseidinsubject

When you send an email from manage case, a hash gets put in the case subject to allow inbound replies to be automatically filed on cases. The system already supports using a regular case id instead of a hash for inbound. For outbound this extension provides a few options:

* Keep it as a hash or change it to the case id.
* Remove it completely, or move it to the end of the subject.
* On the Find Cases page, the case id field will accept a hash.

The extension is licensed under [MIT](LICENSE.txt).

## Requirements

* PHP v7.2+
* CiviCRM 5.29+ (But might work with earlier.)

## Usage

Go to Administer - CiviCase - CiviCase Settings. There will be some new options.

Regardless of the options chosen, the Find Cases page will accept either a case id or a hash.

## Installation (Web UI)

This extension has not yet been published for installation via the web UI.

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl https://lab.civicrm.org/extensions/caseidinsubject/-/archive/master/caseidinsubject-master.zip
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://lab.civicrm.org/extensions/caseidinsubject.git
cv en caseidinsubject
```

## Known Issues

On Find Cases, there's a preexisting issue where if you just enter something that can't possibly be a hash and isn't a number, you get a fatal error. This extension doesn't try to address that.
