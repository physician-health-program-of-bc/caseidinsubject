<?php

require_once 'caseidinsubject.civix.php';
use CRM_Caseidinsubject_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function caseidinsubject_civicrm_config(&$config) {
  _caseidinsubject_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function caseidinsubject_civicrm_xmlMenu(&$files) {
  _caseidinsubject_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function caseidinsubject_civicrm_install() {
  _caseidinsubject_civix_civicrm_install();
  \Civi::settings()->set('caseidinsubject_action', 1);
  \Civi::settings()->set('caseidinsubject_position', 1);
  \Civi::settings()->set('caseidinsubject_prefix', 'case #');
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function caseidinsubject_civicrm_postInstall() {
  _caseidinsubject_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function caseidinsubject_civicrm_uninstall() {
  _caseidinsubject_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function caseidinsubject_civicrm_enable() {
  _caseidinsubject_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function caseidinsubject_civicrm_disable() {
  _caseidinsubject_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function caseidinsubject_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _caseidinsubject_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function caseidinsubject_civicrm_managed(&$entities) {
  _caseidinsubject_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_caseTypes
 */
function caseidinsubject_civicrm_caseTypes(&$caseTypes) {
  _caseidinsubject_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function caseidinsubject_civicrm_angularModules(&$angularModules) {
  _caseidinsubject_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function caseidinsubject_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _caseidinsubject_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function caseidinsubject_civicrm_entityTypes(&$entityTypes) {
  _caseidinsubject_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_civicrm_thems().
 */
function caseidinsubject_civicrm_themes(&$themes) {
  _caseidinsubject_civix_civicrm_themes($themes);
}

/**
 * Implements hook_civicrm_buildForm().
 *
 * Allow searching for the hash on Find Cases.
 */
function caseidinsubject_civicrm_buildForm($formName, $form) {
  if ($formName == 'CRM_Case_Form_Search') {
    $form->add('text', 'case_hash', E::ts('Case Hash'));
    CRM_Core_Region::instance('page-body')->add(array(
      'template' => E::path() . '/templates/CRM/Caseidinsubject/casehash.tpl',
    ));
    return;
  }
}

/**
 * Implements hook_civicrm_preProcess().
 *
 * If it looks like a hash and quacks like a hash then replace with case id.
 */
function caseidinsubject_civicrm_preProcess($formName, &$form) {
  if ($formName == 'CRM_Case_Form_Search' && !empty($form->_submitValues['case_hash'])) {
    // Does it look like a hash?
    if (preg_match('/([0-9a-f]{7})/', $form->_submitValues['case_hash'], $matches)) {
      // Does it quack like a hash?
      $case_id = CRM_Caseidinsubject_Utils::findCorrespondingCase($matches[1]);
      if (!empty($case_id)) {
        // Duck!
        $form->_submitValues['case_id'] = $case_id;
        $form->_formValues['case_id'] = $case_id;
      }
      else {
        // If it's only half a duck we need to do something otherwise it
        // will return all results.
        $form->_submitValues['case_id'] = -1;
        $form->_formValues['case_id'] = -1;
        CRM_Core_Session::setStatus(E::ts('No corresponding case found.'), '', 'alert');
      }
    }
  }
}

/**
 * Implements hook_civicrm_alterMailParams().
 */
function caseidinsubject_civicrm_alterMailParams(&$params, $context) {
  if (empty($params['subject'])) {
    return;
  }
  if (preg_match('/\[case #([0-9a-f]{7})\]/', $params['subject'], $matches)) {
    // strpos can't fail since we already preg_match'd it if we're here. And
    // we know it's near the beginning of the string so can simplify.
    $pos = strpos($params['subject'], ']');
    $params['subject'] = substr($params['subject'], $pos + 2);

    if (\Civi::settings()->get('caseidinsubject_position') == CRM_Caseidinsubject_Utils::POSITION_REMOVE) {
      return;
    }

    // Look it up if setting says to replace with case id
    $case_id = NULL;
    if (\Civi::settings()->get('caseidinsubject_action') == CRM_Caseidinsubject_Utils::ACTION_ID) {
      $case_id = CRM_Caseidinsubject_Utils::findCorrespondingCase($matches[1]);
    }
    // If we couldn't find or setting doesn't say so, keep the original hash.
    if (empty($case_id)) {
      $case_id = $matches[1];
    }

    $prefix_setting = \Civi::settings()->get('caseidinsubject_prefix') ?? 'case #';
    if (\Civi::settings()->get('caseidinsubject_position') == CRM_Caseidinsubject_Utils::POSITION_END) {
      $params['subject'] .= " [{$prefix_setting}{$case_id}]";
    }
    else {
      $params['subject'] = "[{$prefix_setting}{$case_id}] {$params['subject']}";
    }
  }
}
