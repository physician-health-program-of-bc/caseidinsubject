<?php
/* vim: set shiftwidth=2 tabstop=2 softtabstop=2: */

use Civi\Test\HeadlessInterface;
use Civi\Test\HookInterface;
use Civi\Test\TransactionalInterface;

/**
 * FormTest.
 *
 * @group headless
 */
class CRM_Caseidinsubject_FormTest extends \PHPUnit\Framework\TestCase implements HeadlessInterface, HookInterface, TransactionalInterface {

  public function setUpHeadless() {
    return \Civi\Test::headless()->installMe(__DIR__)->apply();
  }

  public function setUp(): void {
    parent::setUp();
    // We need a case with id 1
    CRM_Core_DAO::executeQuery("INSERT INTO civicrm_case_type (id, name, title, is_active) VALUES (1, 'test_casetype', 'test casetype title', 1)");
    CRM_Core_DAO::executeQuery("INSERT INTO civicrm_case (id, subject, start_date, case_type_id, status_id) VALUES (1, 'test case', CURDATE(), 1, 1)");
  }

  public function tearDown(): void {
    CRM_Core_DAO::executeQuery("DELETE FROM civicrm_case");
    CRM_Core_DAO::executeQuery("DELETE FROM civicrm_case_type");
    parent::tearDown();
  }

  /**
   * Test various options for preProcess
   * @dataProvider preProcessProvider
   *
   * @param array $input
   * @param ?int $expected
   *
   */
  public function testPreProcess(array $input, ?int $expected) {
    $form = new CRM_Case_Form_Search();
    $form->_submitValues = $input;
    caseidinsubject_civicrm_PreProcess('CRM_Case_Form_Search', $form);
    $this->assertEquals($expected, $form->_submitValues['case_id'] ?? NULL);
  }

  public function preProcessProvider() {
    return [
      [
        [
          'case_hash' => 'a17c3b4',
        ],
        -1,
      ],

      [
        [
          'case_hash' => 'ede927f',
        ],
        1,
      ],

      [
        [
          'case_hash' => '12345678',
        ],
        -1,
      ],

      [
        [
          'case_hash' => '123456',
        ],
        NULL,
      ],

      [
        [
          'case_hash' => '',
        ],
        NULL,
      ],

      [
        [],
        NULL,
      ],

      [
        [
          'case_id' => NULL,
        ],
        NULL,
      ],

      [
        [
          'case_id' => 1234,
        ],
        1234,
      ],
    ];
  }

}
