<?php
/* vim: set shiftwidth=2 tabstop=2 softtabstop=2: */

use Civi\Test\HeadlessInterface;
use Civi\Test\HookInterface;
use Civi\Test\TransactionalInterface;

/**
 * MailTest.
 *
 * Tips:
 *  - With HookInterface, you may implement CiviCRM hooks directly in the test class.
 *    Simply create corresponding functions (e.g. "hook_civicrm_post(...)" or similar).
 *  - With TransactionalInterface, any data changes made by setUp() or test****() functions will
 *    rollback automatically -- as long as you don't manipulate schema or truncate tables.
 *    If this test needs to manipulate schema or truncate tables, then either:
 *       a. Do all that using setupHeadless() and Civi\Test.
 *       b. Disable TransactionalInterface, and handle all setup/teardown yourself.
 *
 * @group headless
 */
class CRM_Caseidinsubject_MailTest extends \PHPUnit\Framework\TestCase implements HeadlessInterface, HookInterface, TransactionalInterface {

  public function setUpHeadless() {
    // Civi\Test has many helpers, like install(), uninstall(), sql(), and sqlFile().
    // See: https://docs.civicrm.org/dev/en/latest/testing/phpunit/#civitest
    return \Civi\Test::headless()
      ->installMe(__DIR__)
      ->apply();

    // Docs are unclear since it suggests you should be enabling ahead of time in your testing sandbox, but how does that work for headless? I think docs are outdated since it used to be that civix:test would pull the extension status from the actual dev site.
    // Also the installMe() call above I think already does this.
    $manager = \CRM_Extension_System::singleton()->getManager();
    if ($manager->getStatus('caseidinsubject') !== \CRM_Extension_Manager::STATUS_INSTALLED) {
      $manager->install(array('caseidinsubject'));
    }
  }

  public function setUp(): void {
    parent::setUp();
    // We need a case with id 1
    CRM_Core_DAO::executeQuery("INSERT INTO civicrm_case_type (id, name, title, is_active) VALUES (1, 'test_casetype', 'test casetype title', 1)");
    CRM_Core_DAO::executeQuery("INSERT INTO civicrm_case (id, subject, start_date, case_type_id, status_id) VALUES (1, 'test case', CURDATE(), 1, 1)");
  }

  public function tearDown(): void {
    CRM_Core_DAO::executeQuery("DELETE FROM civicrm_case");
    CRM_Core_DAO::executeQuery("DELETE FROM civicrm_case_type");
    parent::tearDown();
  }

  /**
   * Test various options for the subject manipulation
   * @dataProvider mailParamsProvider
   *
   * @param array $input
   * @param array $expected
   *
   */
  public function testAlterMailParams($input, $expected) {
    \Civi::settings()->set('caseidinsubject_action', $this->getConstant($input['action']));
    \Civi::settings()->set('caseidinsubject_position', $this->getConstant($input['position']));
    \Civi::settings()->set('caseidinsubject_prefix', $input['prefix']);
    $params = $input['params'];
    caseidinsubject_civicrm_alterMailParams($params, 'singleEmail');
    $this->assertEquals($expected, $params);
  }

  public function mailParamsProvider() {
    return [
      [
        [
          'action' => 'ACTION_HASH',
          'position' => 'POSITION_REMOVE',
          'prefix' => '',
          'params' => ['subject' => '[case #a17c3b4] Hello there'],
        ],
        [
          'subject' => 'Hello there',
        ],
      ],
      [
        [
          'action' => 'ACTION_HASH',
          'position' => 'POSITION_BEGINNING',
          'prefix' => 'case #',
          'params' => ['subject' => '[case #a17c3b4] Hello there'],
        ],
        [
          'subject' => '[case #a17c3b4] Hello there',
        ],
      ],
      [
        [
          'action' => 'ACTION_HASH',
          'position' => 'POSITION_BEGINNING',
          'prefix' => '',
          'params' => ['subject' => '[case #a17c3b4] Hello there'],
        ],
        [
          'subject' => '[a17c3b4] Hello there',
        ],
      ],
      [
        [
          'action' => 'ACTION_HASH',
          'position' => 'POSITION_END',
          'prefix' => 'case #',
          'params' => ['subject' => '[case #a17c3b4] Hello there'],
        ],
        [
          'subject' => 'Hello there [case #a17c3b4]',
        ],
      ],
      [
        [
          'action' => 'ACTION_HASH',
          'position' => 'POSITION_END',
          'prefix' => '',
          'params' => ['subject' => '[case #a17c3b4] Hello there'],
        ],
        [
          'subject' => 'Hello there [a17c3b4]',
        ],
      ],
      [
        [
          'action' => 'ACTION_ID',
          'position' => 'POSITION_BEGINNING',
          'prefix' => 'case #',
          // Our extension knows when CIVICRM_UF=UnitTests and uses site
          // key 12345, so then this is the hash created for case id 1.
          'params' => ['subject' => '[case #ede927f] Hello there'],
        ],
        [
          'subject' => '[case #1] Hello there',
        ],
      ],
      [
        [
          'action' => 'ACTION_ID',
          'position' => 'POSITION_BEGINNING',
          'prefix' => '',
          'params' => ['subject' => '[case #ede927f] Hello there'],
        ],
        [
          'subject' => '[1] Hello there',
        ],
      ],
      [
        [
          'action' => 'ACTION_ID',
          'position' => 'POSITION_END',
          'prefix' => 'case #',
          'params' => ['subject' => '[case #ede927f] Hello there'],
        ],
        [
          'subject' => 'Hello there [case #1]',
        ],
      ],
      [
        [
          'action' => 'ACTION_ID',
          'position' => 'POSITION_END',
          'prefix' => '',
          'params' => ['subject' => '[case #ede927f] Hello there'],
        ],
        [
          'subject' => 'Hello there [1]',
        ],
      ],
      [
        [
          'action' => 'ACTION_ID',
          'position' => 'POSITION_END',
          'prefix' => 'foo ',
          'params' => ['subject' => '[case #ede927f] Hello there'],
        ],
        [
          'subject' => 'Hello there [foo 1]',
        ],
      ],
    ];
  }

  /**
   * We can't use the constants in the dataprovider because they run
   * before anything has been set up and so the extension constants aren't
   * available yet.
   * So we use corresponding strings in the provider and convert in the
   * test itself by calling this function.
   */
  private function getConstant($constant_string):int {
    switch ($constant_string) {
      case 'ACTION_HASH':
        return CRM_Caseidinsubject_Utils::ACTION_HASH;

      case 'ACTION_ID':
        return CRM_Caseidinsubject_Utils::ACTION_ID;

      case 'POSITION_BEGINNING':
        return CRM_Caseidinsubject_Utils::POSITION_BEGINNING;

      case 'POSITION_END':
        return CRM_Caseidinsubject_Utils::POSITION_END;

      case 'POSITION_REMOVE':
        return CRM_Caseidinsubject_Utils::POSITION_REMOVE;

      default:
        throw new Exception('Unknown constant');
    }
  }

}
