<?php
class CRM_Caseidinsubject_Utils {

  const POSITION_BEGINNING = 1;
  const POSITION_END = 2;
  const POSITION_REMOVE = 3;

  const ACTION_HASH = 1;
  const ACTION_ID = 2;

  /**
   * Given a hash, see if there's a case id that generates the same hash.
   *
   * @param string $hash
   *
   * @return string|NULL
   */
  public static function findCorrespondingCase(string $hash): ?string {
    $site_key = CRM_Core_DAO::escapeString(CIVICRM_UF === 'UnitTests' ? '12345' : CIVICRM_SITE_KEY);
    $sql_params = array(1 => array($hash, 'String'));
    return CRM_Core_DAO::singleValueQuery("SELECT id FROM civicrm_case WHERE SUBSTR(SHA1(CONCAT('$site_key', id)), 1, 7) = %1", $sql_params);
  }

}
