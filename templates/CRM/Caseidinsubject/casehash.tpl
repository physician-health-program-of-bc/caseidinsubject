<table id="delete-me-later">
  <tr colspan="2" class="crm-case-common-form-row-case_hash">
    <td class="crm-case-common-form-block-case_hash">
      <label for="case_hash">{$form.case_hash.label}</label>
      <br>
      {$form.case_hash.html}
    </td>
  </tr>
</table>
<script type="text/javascript">
{literal}
(function($) {
  $('tr.crm-case-common-form-row-case_hash').insertAfter($('td.crm-case-common-form-block-case_id').parent());
  $('table#delete-me-later').remove();
})(CRM.$);
{/literal}
</script>
